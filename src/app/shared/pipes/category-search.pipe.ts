import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categorySearch'
})
export class CategorySearchPipe implements PipeTransform {

  transform( category :any[],term:string): any {
    if (term==undefined){
      return category;
    }
    return category.filter(function(category){
      return  category.aName.includes(term)
    })
  }  

}
