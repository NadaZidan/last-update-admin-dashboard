import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderSearch'
})
export class OrderSearchPipe implements PipeTransform {

  transform( order :any[],term:string): any {
    if (term==undefined){
      return order;
    }
    return order.filter(function(order){
      return  order.user.name.includes(term)
    })
  }

}
