import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { OrderSearchPipe } from './order-search.pipe';
import { CategorySearchPipe } from './category-search.pipe';

@NgModule({
    imports: [CommonModule],
    declarations: [OrderSearchPipe, CategorySearchPipe]
})
export class SharedPipesModule {}
