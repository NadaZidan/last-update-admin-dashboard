import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Post } from '../model/post';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  apiUrl = "https://api.egypt-youth.com:4430/api/ProJectTypes"
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(private http: HttpClient) { }

  getAll(pageNumbers): Observable<any> {
    return this.http.get("https://api.egypt-youth.com:4430/api/ProJectTypes?pageCount="+pageNumbers)
      .pipe(
        catchError(this.handleError)
      );
  }
  create(post): Observable<Post> {
    return this.http.post<Post>(this.apiUrl, JSON.stringify(post), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }
  find(id): Observable<Post> {
    return this.http.get<Post>(`${this.apiUrl}/${id}`)
      .pipe(
        catchError(this.handleError)
      )
  }
  getForm(id): Observable<Post> {
    return this.http.get<Post>(`https://api.egypt-youth.com:4430/api/ProForms/${id}`)
      .pipe(
        catchError(this.handleError)
      )
  }

  update(id, post): Observable<Post> {
    return this.http.put<Post>(`${this.apiUrl}/${id}`, JSON.stringify(post), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  delete(id) {
    return this.http.delete<Post>(`${this.apiUrl}/${id}`, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.log(error.error.message)

    } else {
      console.log(error.status)
    }
    return throwError(
      console.log('Something is wrong!'));
  };






}
