import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  apiUrl="https://api.egypt-youth.com:4430/api/Report/currentPage"
  constructor(private http: HttpClient,
  ) { }
  
  
  
    getAll(): Observable<any> {
      return this.http.get(this.apiUrl)
        .pipe(
          catchError(this.handleError)
        );
    }
  
    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        console.log(error.error.message)
  
      } else {
        console.log(error.status)
      }
      return throwError(
        console.log('Something is wrong!'));
    };
  
  }
  

