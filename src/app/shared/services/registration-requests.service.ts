import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegistrationRequestsService {

  apiUrl = 'https://api.egypt-youth.com:4430/api/Acount/Seallers/Requst'

  constructor(
    private http: HttpClient,
  ) { }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  getAll(): Observable<any> {
    return this.http.get(this.apiUrl)
      .pipe(
        catchError(this.handleError)
      );
  }
  verify(acountId: string): Observable<any> {
    let body = {
      "seallerId": acountId,
      "confierm": true
    }
    return this.http.post<any>("https://api.egypt-youth.com:4430/api/Acount/Sealler/Confierm", JSON.stringify(body), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.log(error.error.message)

    } else {
      console.log(error.status)
    }
    return throwError(
      console.log('Something is wrong!'));
  };

}
