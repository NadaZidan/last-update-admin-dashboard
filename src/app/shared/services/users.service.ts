import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserBlock } from '../model/userBlock';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  apiUrl = "https://api.egypt-youth.com:4430/api/Acount/block"
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(
    private http: HttpClient,
  ) { }

  getAll(): Observable<any> {
    return this.http.get(`${this.apiUrl}`)
      .pipe(
        catchError(this.handleError)
      );
  }
  block(acountId: number): Observable<any> {
    let body = {
      "acountId": acountId,
      "block": true
    }

    return this.http.post<any>(`${this.apiUrl}`, JSON.stringify(body), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }
  unBlock(acountId: number): Observable<any> {
    let body = {
      "acountId": acountId,
      "block": false
    }
    return this.http.post<any>(`${this.apiUrl}`, JSON.stringify(body), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }




  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.log(error.error.message)

    } else {
      console.log(error.status)
    }
    return throwError(
      console.log('Something is wrong!'));
  };
}
