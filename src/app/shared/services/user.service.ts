import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl= 'https://api.egypt-youth.com:4430/api/Acount/login'
  constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(this.apiUrl);
    }
    

    
}
