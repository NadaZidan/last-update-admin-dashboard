import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {  User } from './shared/model/user';
import { AuthService } from './shared/services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    currentUser: User;

    constructor(
        private router: Router,
        private authService:AuthService
    ) {
        this.authService.currentUser.subscribe(x => this.currentUser = x);
    }
    ngOnInit(): void {
    }

    get isAdmin() {
        return this.currentUser && this.currentUser.role;
    }

   
}

