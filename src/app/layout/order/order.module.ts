import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order.component';
import { NgbAlertModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { OrderRoutingModule } from './order-routing.module';
import { StatModule } from '../../shared/modules';
import { FormsModule } from '@angular/forms';
import { OrderSearchPipe } from '../../shared/pipes/order-search.pipe';
import { TranslateModule } from '@ngx-translate/core';




@NgModule({
  declarations: [OrderComponent, OrderSearchPipe],
  imports: [OrderRoutingModule,NgbAlertModule, NgbCarouselModule,StatModule,TranslateModule,
    CommonModule, FormsModule]
})
export class OrderModule { }
