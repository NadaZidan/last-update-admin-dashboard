import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../../router.animations';
import { OrderService } from "../../shared/services/order.service"

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  animations: [routerTransition()],



})
export class OrderComponent implements OnInit {
term:string
  data= []

  constructor(
    private orderSrvice: OrderService
  ) {
    this.orderSrvice.getAll().subscribe((result) => {
      this.data = result.data;
      console.log('JSON Response = ', this.data);
    })
  }



  ngOnInit(): void {

  }
  getOrderStatusText(orderStatus: number): string {
    let status: string;
    switch (orderStatus) {
      case 0:
        status = "Ordered";
        break;
      case 1:
        status = "Rejected";
        break;
      case 2:
        status = "Approved";
        break;
      case 3:
        status = "Finshed";
        break;
      case 4:
        status = "Shipped";
        break;
      case 5:
        status = "Delivered";
        break;
      default:
        status = "Unkown";
        break;
    }
    return status;

  }
 }
