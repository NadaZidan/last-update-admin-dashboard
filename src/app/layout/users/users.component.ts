import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { UsersService } from '../../shared/services/users.service';
import { UserBlock } from '../../shared/model/userBlock';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  animations: [routerTransition()]

})
export class UsersComponent implements OnInit {
  data = [];
  




  constructor(private usersSrvice: UsersService) {
    this.getAllUsers();
  }

  getAllUsers() {
    this.usersSrvice.getAll().subscribe((result) => {
      this.data = result.data;
      console.log('JSON Response = ', this.data);
    })

  }

  block(id: number) {

    this.usersSrvice.block(id).subscribe((result: any) => {
      this.getAllUsers();
      console.log(result)
    })
  }


  unBlock(id: number) {
    this.usersSrvice.unBlock(id).subscribe((result: any) => {
      console.log(result)
      this.getAllUsers();
    })
  }
  








  ngOnInit(): void { }

}
