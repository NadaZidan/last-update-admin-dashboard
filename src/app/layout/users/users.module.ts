import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbAlertModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { StatModule } from '../../shared/modules';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [UsersComponent],
  imports: [UsersRoutingModule,NgbAlertModule, NgbCarouselModule,StatModule,TranslateModule,
    CommonModule
  ]
})
export class UsersModule { }
