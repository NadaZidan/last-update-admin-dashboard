import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../../router.animations';
import { ProductService } from '../../shared/services/product.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  animations: [routerTransition()]

})
export class ProductsComponent implements OnInit {
  data = [
   
   ];

   constructor(
    private productSrvice: ProductService 
    ){ 
      this.productSrvice.getAll().subscribe((result) =>  {
        this.data = result.data;
      console.log('JSON Response = ', this.data);
      })
    }
  
   
    

  ngOnInit(): void {
  }

 

}
