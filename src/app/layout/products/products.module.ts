import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbAlertModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { TranslateModule } from '@ngx-translate/core';


import { StatModule } from '../../shared/modules';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { ProductsdetailsComponent } from './productsdetails/productsdetails.component';


@NgModule({
  declarations: [ProductsComponent, ProductsdetailsComponent],
  imports: [ProductsRoutingModule,NgbAlertModule, NgbCarouselModule,StatModule, TranslateModule,
    CommonModule,CarouselModule, ]
})
export class ProductsModule { }
