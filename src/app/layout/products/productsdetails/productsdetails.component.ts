import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../shared/services/product.service';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-productsdetails',
  templateUrl: './productsdetails.component.html',
  styleUrls: ['./productsdetails.component.css']
})
export class ProductsdetailsComponent implements OnInit {


  data: [];
  selectedProduct: any;
  id: number;
  constructor(private productSrvice: ProductService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.id = this.route.snapshot.params['id'];
    this.productSrvice.getAll().subscribe((result) => {
      this.data = result.data;
      this.selectedProduct = this.data.find((element: any) => element.id == this.id);
      console.log('JSON Response = ', this.data);
      console.log("Selected product: ", this.selectedProduct);
    })



  }
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  }







  ngOnInit(): void {
  }

}
