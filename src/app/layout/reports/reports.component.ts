import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { ReportsService } from '../../shared/services/reports.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
  animations: [routerTransition()]

})
export class ReportsComponent implements OnInit {
  data = [
   
   ];
   constructor(
    private reportsSrvice: ReportsService 
    ){ 
      this.reportsSrvice.getAll().subscribe((result) =>  {
        this.data = result.data;
      console.log('JSON Response = ', this.data);
      })
    }
  ngOnInit(): void {
  }

}
