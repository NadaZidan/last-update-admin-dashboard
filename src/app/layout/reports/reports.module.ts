import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsRoutingModule } from './reports-routing.module';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { ReportsComponent } from './reports.component';
import { TranslateModule } from '@ngx-translate/core';




@NgModule({
  declarations: [ReportsComponent],
  imports: [NgbCarouselModule,ReportsRoutingModule,NgbAlertModule,TranslateModule,
    
    CommonModule
  ]
})
export class ReportsModule { }
