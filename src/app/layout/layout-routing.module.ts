import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { ProductsdetailsComponent } from './products/productsdetails/productsdetails.component';
import { AddCategoryComponent } from './category/add-category/add-category.component';
import { EditCategoryComponent } from './category/edit-category/edit-category.component';


const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule)
            },
            { path: '', redirectTo: 'products', pathMatch: 'prefix' },
            {
                path: 'products',
                loadChildren: () => import('./products/products.module').then((m) => m.ProductsModule)
            },
            { path: 'products-details/:id', component: ProductsdetailsComponent },



            { path: '', redirectTo: 'category', pathMatch: 'prefix' },
            {
                path: 'category',
                loadChildren: () => import('./category/category.module').then((m) => m.CategoryModule)
            },
            { path: 'add-category', component: AddCategoryComponent },
            { path: 'edit-category/:id', component: EditCategoryComponent },


            { path: '', redirectTo: 'order', pathMatch: 'prefix' },
            {
                path: 'order',
                loadChildren: () => import('./order/order.module').then((m) => m.OrderModule)
            },


            {
                path: 'users',
                loadChildren: () => import('./users/users.module').then((m) => m.UsersModule)
            },
            { path: '', redirectTo: 'contacts', pathMatch: 'prefix' },



            {
                path: 'reports',
                loadChildren: () => import('./reports/reports.module').then((m) => m.ReportsModule)
            },

            { path: '', redirectTo: 'registration requests', pathMatch: 'prefix' },

            {
                path: 'registration requests',
                loadChildren: () => import('./registration-requests/registration-requests.module').then((m) => m.RegistrationRequestsModule)
            },
            { path: '', redirectTo: 'cities', pathMatch: 'prefix' },

            {
                path: 'cities',
                loadChildren: () => import('./cities/cities.module').then((m) => m.CitiesModule )
            },


        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
