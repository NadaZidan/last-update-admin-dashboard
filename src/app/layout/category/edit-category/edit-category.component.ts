import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm, FormArray } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CategoryService } from '../../../shared/services/category.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from '../../../shared/model/post';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent implements OnInit {

  EditCategoryForm: FormGroup;
  public proForm: FormArray



  id: number;
  post: Post;

  constructor(
    public categoryService: CategoryService,
    private route: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.categoryService.find(this.id).subscribe((data: Post) => {
      this.post = data;
      this.EditCategoryForm = this.fb.group({
        id: (parseInt(this.id.toString())),
        name: ([this.post.name, Validators.required]),
        aName: ([this.post.aName, Validators.required]),

        proForm: this.fb.array([
        ])


      });

    });
    this.categoryService.getForm(this.id).subscribe((data: any) => {
      console.log(data);
      let control = <FormArray>this.EditCategoryForm.controls.proForm;
      for (let i = 0; i < data.length; i++) {
        control.push(
          this.fb.group({
            name: ([data[i].name, Validators.required]),
            aName: ([data[i].aName, Validators.required]),
            required: (data[i].required),
            type: (data[i].type),
            id: (data[i].id),
            proJectTypeId: (parseInt(this.id.toString()))
          })
        )
      }
    }, (error) => {
      console.log("Get form error");
      console.log(error);
    });





  }
  removeOption(i: number) {
    let control = <FormArray>this.EditCategoryForm.controls.proForm;
    control.removeAt(i);
  }

  setOptions() {
    return this.fb.group({
      id: (''),
      name: (['', Validators.required]),
      aName: (['', Validators.required]),
      required: (true),
      type: (0),

    })

  }
  addNewField(): void {

    let control = <FormArray>this.EditCategoryForm.controls.proForm;
    control.push(
      this.fb.group({
        name: (['', Validators.required]),
        aName: (['', Validators.required]),
        required: (true),
        type: (0),
        proJectTypeId: (parseInt(this.id.toString()))
      })
    )
  }


  submit() {
    this.categoryService.update(this.id, this.EditCategoryForm.value).subscribe(res => {
      console.log('updated successfully!');
      this.router.navigateByUrl('/category');

    })
  }

}





