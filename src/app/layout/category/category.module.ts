import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbAlertModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { StatModule } from '../../shared/modules';
import { CategoryRoutingModule } from './category-routing.module';
import { CategoryComponent } from './category.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { JwPaginationComponent } from 'jw-angular-pagination';

import { EditCategoryComponent } from './edit-category/edit-category.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CategorySearchPipe } from '../../shared/pipes/category-search.pipe';
import { TranslateModule } from '@ngx-translate/core';







@NgModule({
  declarations: [CategoryComponent, AddCategoryComponent, EditCategoryComponent,CategorySearchPipe,    JwPaginationComponent
  ],
  imports: [NgbAlertModule, NgbCarouselModule,StatModule,CategoryRoutingModule, ModalModule.forRoot()
    ,
    CommonModule,FormsModule,TranslateModule,
    ReactiveFormsModule, 

  ],
  entryComponents:[AddCategoryComponent,  EditCategoryComponent],

})
export class CategoryModule { }
