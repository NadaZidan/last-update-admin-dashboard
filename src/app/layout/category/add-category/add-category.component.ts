import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CategoryService } from '../../../shared/services/category.service';
import { Router } from '@angular/router';


@Component({

  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css'],

})
export class AddCategoryComponent implements OnInit {
  Post = {
    id: '',
    name: '',
    aName: '',
    proForm: [
      {
        id: '',
        aName: "",



        name: "",
        type: 0,

        required: true
      }

    ]
  }
  private router: Router
  public proForm:FormArray
  public show:boolean=false;
 
  public addNewCategoryForm: FormGroup;
  constructor(public fb: FormBuilder, public categoryService: CategoryService) {


  }
  ngOnInit() {

    
    this.addNewCategoryForm = this.fb.group({


      name: (['', Validators.required]),
      aName: (['', Validators.required]),

      proForm: this.fb.array([

      ])


    });
    this.setOptions()


  }


  addNewField(): void {
    let control = <FormArray>this.addNewCategoryForm.controls.proForm;
    control.push(
      this.fb.group({
        name: (['', Validators.required]),
        aName: (['', Validators.required]),
        required: (true),
        type: (0),

      })
    )
    this.show=!this.show;

  }
  setOptions() {
    
    let control = <FormArray>this.addNewCategoryForm.controls.proForm;
    this.Post.proForm.forEach(x => {
      control.push(this.fb.group({
        name: x.name,
        aName: x.aName,
        type: x.type,
        required: x.required

      }))
    })
  }


  removeOption(i: number) {
    let control = <FormArray>this.addNewCategoryForm.controls.proForm;
    control.removeAt(i);
  }

  logValue() {
    console.log(this.addNewCategoryForm.value);


    this.categoryService.create(this.addNewCategoryForm.value).subscribe(res => {
      this.router.navigateByUrl('/category');
    })
  }


}



















