import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { AddCategoryComponent } from './add-category/add-category.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { CategoryService } from '../../shared/services/category.service';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  animations: [routerTransition()]

})
export class CategoryComponent implements OnInit {
  term:string

  currentPage = 1;
  

  pageNumbers:number[]=[];

  data = [
   
  ];

  constructor(private categorySrvice: CategoryService) {
    for(let i =1 ; i <11 ; i++)
    {
      this.pageNumbers.push(i);
    }
    this.categorySrvice.getAll(this.currentPage).subscribe((result) => {
      
     this.data = result.data;

      console.log('JSON Response = ', this.data);
    })



  }
  changeNumber(ind)
  {
    this.currentPage  = ind;
    this.categorySrvice.getAll(this.currentPage).subscribe((result) => {
      
      this.data = result.data;
 
       console.log('JSON Response = ', this.data);
     })
   
  }
  prev()
  {
     this.changeNumber(this.currentPage -1)
  }
  next()
  {
     this.changeNumber(this.currentPage +1)
  }

  deletePost(id) {
    this.categorySrvice.delete(id).subscribe(res => {
      this.data = this.data.filter(item => item.id !== id);
      alert('Category deleted successfully!');
    }, (error) => {
      alert("There are producers that use this category");
    })
  }


 
  


  ngOnInit(): void {
  }

}
