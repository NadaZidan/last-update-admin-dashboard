import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationRequestsComponent } from './registration-requests.component';
import { NgbAlertModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { RegistrationRequestsRoutingModule } from './registrationRrequests-routing.module';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [RegistrationRequestsComponent],
  imports: [NgbAlertModule, NgbCarouselModule,RegistrationRequestsRoutingModule,TranslateModule,
    CommonModule
  ]
})
export class RegistrationRequestsModule { }
