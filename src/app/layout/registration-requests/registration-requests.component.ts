import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { RegistrationRequestsService } from '../../shared/services/registration-requests.service';

@Component({
  selector: 'app-registration-requests',
  templateUrl: './registration-requests.component.html',
  styleUrls: ['./registration-requests.component.css'],
  animations: [routerTransition()],

})
export class RegistrationRequestsComponent implements OnInit {
  data = [

  ];
  constructor(
    private registrationSrvice: RegistrationRequestsService
  ) {
    this.getALLData()
  }

  getALLData() {
    this.registrationSrvice.getAll().subscribe((result) => {
      this.data = result.data;
      console.log('JSON Response = ', this.data);
    })
  }
  verify(id: string) {
    this.registrationSrvice.verify(id).subscribe((result) => {
      console.log(result);
      this.getALLData()
    })
  }
  ngOnInit(): void {
  }

}
