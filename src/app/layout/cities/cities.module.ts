import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbAlertModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { StatModule } from '../../shared/modules';
import { CitiesRoutingModule } from './cities-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [],
  imports: [NgbAlertModule, NgbCarouselModule,StatModule,CitiesRoutingModule, ModalModule.forRoot()
    , CommonModule,FormsModule,TranslateModule,ReactiveFormsModule
  ]
})
export class CitiesModule { }
